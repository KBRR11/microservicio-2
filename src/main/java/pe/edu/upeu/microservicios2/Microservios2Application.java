package pe.edu.upeu.microservicios2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Microservios2Application {

	public static void main(String[] args) {
		SpringApplication.run(Microservios2Application.class, args);
	}

}

package pe.edu.upeu.microservicios2.modelo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import pe.edu.upeu.microservicios2.modelo.entity.Registro;
import pe.edu.upeu.microservicios2.modelo.service.RegistroDao;

@RestController
public class RegistroController {
@Autowired
private RegistroDao registroDao;

@GetMapping("/listar")
public List<Registro> listar(){
	return registroDao.readAll();
}

@GetMapping("/listar/{id}")
public Registro registrar(@PathVariable Long id) {
	return registroDao.readById(id);
}
}

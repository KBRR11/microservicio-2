package pe.edu.upeu.microservicios2.modelo.service;

import java.util.List;

import pe.edu.upeu.microservicios2.modelo.entity.Alumno;
import pe.edu.upeu.microservicios2.modelo.entity.Registro;

public interface RegistroDao {
public List<Registro> readAll();
public Registro readById(Long id);
}

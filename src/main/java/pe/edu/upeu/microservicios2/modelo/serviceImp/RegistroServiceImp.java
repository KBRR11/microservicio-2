package pe.edu.upeu.microservicios2.modelo.serviceImp;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import pe.edu.upeu.microservicios2.modelo.entity.Alumno;
import pe.edu.upeu.microservicios2.modelo.entity.Registro;
import pe.edu.upeu.microservicios2.modelo.service.RegistroDao;
@Service
public class RegistroServiceImp implements RegistroDao {
@Autowired
private RestTemplate clienteRestTemplate;

	@Override
	public List<Registro> readAll() {
		List<Alumno> alumnos = Arrays.asList(clienteRestTemplate.getForObject("http://localhost:9001/listar", Alumno[].class));
		return alumnos.stream().map(a->new Registro(a)).collect(Collectors.toList());
	}

	@Override
	public Registro readById(Long id) {
		Map<String, String> datos = new HashMap<String, String>();
		datos.put("id", id.toString());
		Alumno alumno = clienteRestTemplate.getForObject("http://localhost:9001/listar/{id}", Alumno.class,datos);
		return new Registro(alumno);
	}

}

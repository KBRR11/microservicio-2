package pe.edu.upeu.microservicios2.modelo.entity;

public class Registro {
private Alumno alumno;

public Registro() {
	super();
	// TODO Auto-generated constructor stub
}

public Registro(Alumno alumno) {
	
	this.alumno = alumno;
}

public Alumno getAlumno() {
	return alumno;
}

public void setAlumno(Alumno alumno) {
	this.alumno = alumno;
}



}
